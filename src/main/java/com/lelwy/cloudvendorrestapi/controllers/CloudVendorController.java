package com.lelwy.cloudvendorrestapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lelwy.cloudvendorrestapi.models.CloudVendor;
import com.lelwy.cloudvendorrestapi.responses.ResponseHandler;
import com.lelwy.cloudvendorrestapi.services.CloudVendorService;

@RestController
@RequestMapping("/cloudVendor")
public class CloudVendorController {
	
	@Autowired
	CloudVendorService cloudVendorService;

	// Read specific cloud vendor details from DB
	@GetMapping("{vendorId}")
	public ResponseEntity<Object> getCloudVendorDetails(@PathVariable("vendorId") String vendorId) {
	
		return ResponseHandler.responseBuilder(
					"Requested vendor details are given here",
					HttpStatus.OK,
					cloudVendorService.getCloudVendor(vendorId)
					);
	}

	// Read all cloud vendors details from DB
	@GetMapping()
	public ResponseEntity<Object> getAllCloudVendorDetails() {
	
		return ResponseHandler.responseBuilder("All vendors details are given here",
				HttpStatus.OK,
				cloudVendorService.getAllCloudVendors());
	}
	
	@PostMapping
	public String createCloudVendorDetails(@RequestBody CloudVendor cloudVendor) {
		
		return cloudVendorService.createCloudVendor(cloudVendor);
	}
	
	@PutMapping
	public String updateCloudVendorDetails(@RequestBody CloudVendor cloudVendor) {
		
		return cloudVendorService.updateCloudVendor(cloudVendor);
	}
	
	@DeleteMapping("{vendorId}")
	public String deleteCloudVendorDetails(@PathVariable("vendorId") String vendorId) {
		
		return cloudVendorService.deleteCloudvendor(vendorId);
	}
}
