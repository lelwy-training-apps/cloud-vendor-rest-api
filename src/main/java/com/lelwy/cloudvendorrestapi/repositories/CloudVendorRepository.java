package com.lelwy.cloudvendorrestapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lelwy.cloudvendorrestapi.models.CloudVendor;

public interface CloudVendorRepository extends JpaRepository<CloudVendor, String>{

}
