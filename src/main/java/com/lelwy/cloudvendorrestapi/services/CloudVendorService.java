package com.lelwy.cloudvendorrestapi.services;

import java.util.List;

import com.lelwy.cloudvendorrestapi.models.CloudVendor;

public interface CloudVendorService {

	public String createCloudVendor(CloudVendor cloudVendor);
	
	public String updateCloudVendor(CloudVendor cloudVendor);
	
	public String deleteCloudvendor	(String cloudVendorId);
	
	public CloudVendor getCloudVendor(String cloudVendorId);
	
	public List<CloudVendor> getAllCloudVendors();
}
