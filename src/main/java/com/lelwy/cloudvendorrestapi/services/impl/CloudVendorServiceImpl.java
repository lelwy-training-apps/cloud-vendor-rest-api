package com.lelwy.cloudvendorrestapi.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lelwy.cloudvendorrestapi.exceptions.CloudVendorNotFoundException;
import com.lelwy.cloudvendorrestapi.models.CloudVendor;
import com.lelwy.cloudvendorrestapi.repositories.CloudVendorRepository;
import com.lelwy.cloudvendorrestapi.services.CloudVendorService;

@Service
public class CloudVendorServiceImpl implements CloudVendorService{
	
	
	@Autowired
	CloudVendorRepository cloudVendorRepository;

	@Override
	public String createCloudVendor(CloudVendor cloudVendor) {

		cloudVendorRepository.save(cloudVendor);
		
		return "Success";
	}

	@Override
	public String updateCloudVendor(CloudVendor cloudVendor) {

		cloudVendorRepository.save(cloudVendor);
		
		return "Success";
	}

	@Override
	public String deleteCloudvendor(String cloudVendorId) {

		cloudVendorRepository.deleteById(cloudVendorId);
		return "Success";
	}

	@Override
	public CloudVendor getCloudVendor(String cloudVendorId) {

		if(cloudVendorRepository.findById(cloudVendorId).isEmpty())
			throw new CloudVendorNotFoundException("Requested cloud vendor does not exist");
		return cloudVendorRepository.findById(cloudVendorId).get();
	}

	@Override
	public List<CloudVendor> getAllCloudVendors() {

		return cloudVendorRepository.findAll();
	}

}
